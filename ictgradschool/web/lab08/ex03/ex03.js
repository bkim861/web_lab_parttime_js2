"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    number *= 100;
    number = Math.round(number);
    return number / 100;
}

// TODO Write a function which calculates and returns the volume of a cone.
function getVolumeOfCone(radius, height){
    return Math.PI * radius * radius * height / 3.0;
}

// TODO Write a function which calculates and returns the volume of a cylinder.


// TODO Write a function which prints the name and volume of a shape, to 2dp.


// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.
