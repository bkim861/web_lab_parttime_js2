"use strict";
var sum = 0;
var addedNumbers = 0;

for(var i = 1; i <= 20; i++){
    if (i % 2 == 0){
        sum += i;
        addedNumbers += 1;
    }
}
console.log(sum / addedNumbers);