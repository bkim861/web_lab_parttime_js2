"use strict";
var sum = 0;
var i = 1;
var numbersAdded = 0;

while(i <= 20){
    if(i % 2 == 0){
        sum += i;
        numbersAdded++;
    }
    i++;
}
console.log(sum / numbersAdded);
